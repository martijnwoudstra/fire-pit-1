package jodelahithit.mods.firepit.lib;

public class Ref {
	
	public static final String MODID = "firepit";
	public static final String MODNAME = "Fire Pit";
	public static final String VERSION = "1.0-1.7.2";
	public static final String CLIENTSIDE = "jodelahithit.mods.firepit.proxy.ClientProxy";
	public static final String SERVERSIDE = "jodelahithit.mods.firepit.proxy.CommonProxy";
	
}
