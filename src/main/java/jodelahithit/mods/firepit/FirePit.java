package jodelahithit.mods.firepit;

import jodelahithit.mods.firepit.blocks.BlockFirePit;
import jodelahithit.mods.firepit.lib.Ref;
import jodelahithit.mods.firepit.misc.CreativeTab;
import jodelahithit.mods.firepit.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;



@Mod(modid = Ref.MODID, name = Ref.MODNAME, version = Ref.VERSION)
public class FirePit{

    public static CreativeTabs CT = new CreativeTab(CreativeTabs.getNextID(), "Fire Pit");

    @SidedProxy(serverSide = Ref.SERVERSIDE, clientSide = Ref.CLIENTSIDE)
    public static CommonProxy proxy;

    public static Block FirePit = new BlockFirePit(Material.ground);
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e){

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e){
        GameRegistry.registerBlock(FirePit, "FirePit");
    	GameRegistry.addShapedRecipe(new ItemStack(this.FirePit), new Object[] { "yxy", "xXx", "yxy", 'x', Items.stick, 'X', Blocks.log, 'y', Blocks.dirt});
    }
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e){

    }
}