package jodelahithit.mods.firepit.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jodelahithit.mods.firepit.FirePit;
import jodelahithit.mods.firepit.lib.Ref;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import static net.minecraftforge.common.util.ForgeDirection.UP;

public class BlockFirePit extends Block{

    protected IIcon blockIcon;
    protected IIcon blockIconTop;

    public BlockFirePit(Material material){
        super(material);
        this.setCreativeTab(FirePit.CT);
        this.setBlockName("FirePit");
        this.setBlockTextureName("FirePit");
    }

    @Override
    public boolean onBlockActivated(World p_149727_1_, int p_149727_2_, int p_149727_3_, int p_149727_4_, EntityPlayer p_149727_5_, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_){
        return super.onBlockActivated(p_149727_1_, p_149727_2_, p_149727_3_, p_149727_4_, p_149727_5_, p_149727_6_, p_149727_7_, p_149727_8_, p_149727_9_);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister icon){

        blockIcon = icon.registerIcon("minecraft:dirt");
        blockIconTop = icon.registerIcon(Ref.MODID + ":" + this.getTextureName() + "Top");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int metadata){
        if(side == 1){
            return blockIconTop;
        }
        else{
            return blockIcon;
        }
    }

    public boolean isFireSource(World world, int x, int y, int z, ForgeDirection side){
        if(side == UP){
            return true;
        }

        return false;
    }

}