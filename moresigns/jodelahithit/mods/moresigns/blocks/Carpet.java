package jodelahithit.mods.moresigns.blocks;

import jodelahithit.mods.moresigns.MoreSigns;
import jodelahithit.mods.moresigns.lib.Ref;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class Carpet extends Block {

	public Carpet(Material material) {
		super(material);

	    this.setCreativeTab(MoreSigns.CT);
	    this.setHardness(1f);
	    this.setStepSound(soundTypeGrass);
	    this.setBlockTextureName(Ref.MODID + ":" + "title");
	    this.setLightOpacity(1);
	}
	    @Override
	public boolean isOpaqueCube() {
	        return false;
	}
}
