package jodelahithit.mods.moresigns.blocks;

import java.util.Random;

import jodelahithit.mods.moresigns.MoreSigns;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class Teleport extends Item {

	private boolean active = false;
	private Random rand;
	public Teleport() {
		super();
		this.setCreativeTab(MoreSigns.CT);
		this.setUnlocalizedName("Teleporter");
	}

	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		// int x = (int) player.posX;
		// int y = (int) player.posY;
		// int z = (int) player.posZ;
		// if (active == true) {
		// System.out.println("off");
		// active = false;
		// }
		// if (active == false) {
		// System.out.println("on");
		// active = true;
		// }
		// if (player.worldObj.getBlock(x, y - 1, z) != Blocks.air){
		// System.out.println("y");
		// }
		// if (active == true || player.worldObj.getBlock(x, y - 1, z) !=
		// Blocks.sand) {
		// player.jump();
		//
		//
		// }
		if (player.ridingEntity != null)
			return itemStack;
		else {
			if (player.worldObj.provider.isSurfaceWorld()) {
				int x = world.getSpawnPoint().posX;
				int y = world.getSpawnPoint().posY;
				int z = world.getSpawnPoint().posZ;
				itemStack.damageItem(1, player);
				player.setPosition(x, y + 6, z);
	                world.spawnParticle("hugeexplosion", (double)(x), (double)y + 6, (double)(z), 0.0D, 0.0D, 0.0D);

			}
			return itemStack;
		}
	}
}
