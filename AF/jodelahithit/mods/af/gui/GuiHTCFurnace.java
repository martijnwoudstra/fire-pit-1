//package jodelahithit.mods.af.gui;
//
//import jodelahithit.mods.af.AF;
//import jodelahithit.mods.af.tileEntity.TileEntityHTCFurnace;
//import net.minecraft.client.gui.inventory.GuiContainer;
//import net.minecraft.client.resources.I18n;
//import net.minecraft.entity.player.InventoryPlayer;
//import net.minecraft.util.ResourceLocation;
//
//
//public class GuiHTCFurnace extends GuiContainer {
//
//    public static final ResourceLocation texture = new ResourceLocation(AF.modid, ":" + "textures/gui/" + "furnace.png");
//    
//    public TileEntityHTCFurnace HTCFurnace;
//    
//    public GuiHTCFurnace(InventoryPlayer inventoryPlayer, TileEntityHTCFurnace entity) {
//        super(new ContainerHTCFurnace(inventoryPlayer, entity));
//        
//        this.HTCFurnace = entity;
//        
//        this.xSize = 176;
//        this.ySize = 166;
//    }
//    
//    public void drawGuiContainerForegroundLayer(float par1, int par2) {
//        String name = this.HTCFurnace.isInvNameLocalized();
//        
//        this.fontRenderer.drawString(I18n.getString("container.inventory"), 8, this.ySize -94, 4210752);
//
//    }
//    public void drawGuiContainerBackgroundLayer(float f, int i, int j) {
//        
//    }
//
//}
