//package jodelahithit.mods.af.gui;
//
//import jodelahithit.mods.af.AF;
//import jodelahithit.mods.af.tileEntity.TileEntityHTCFurnace;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.world.World;
//import cpw.mods.fml.common.network.IGuiHandler;
//
//public class GuiHandler implements IGuiHandler {
//
//    @Override
//    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
//        TileEntity entity = world.getBlockTileEntity(x, y, z);
//        
//        if (entity != null) {
//            switch (ID) {
//                case AF.guiIdHTCFurnace:
//                    if (entity instanceof TileEntityHTCFurnace) {
//                        return new ContainerHTCFurnace(player.inventory, (TileEntityHTCFurnace) entity);
//                    }
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
//        TileEntity entity = world.getBlockTileEntity(x, y, z);
//
//        if (entity != null) {
//            switch (ID) {
//                case AF.guiIdHTCFurnace:
//                    if (entity instanceof TileEntityHTCFurnace) {
//                        return new GuiHTCFurnace(player.inventory, (TileEntityHTCFurnace) entity);
//                    }
//            }
//        }
//        return null;
//    }
//
//}
