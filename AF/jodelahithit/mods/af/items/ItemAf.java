package jodelahithit.mods.af.items;

import jodelahithit.mods.af.AF;
import jodelahithit.mods.af.lib.IAF;
import jodelahithit.mods.af.lib.ModInfo;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ItemAf extends Item implements IAF {
	
	public ItemAf(){

        setUnlocalizedName(getName());
        setCreativeTab(AF.CT);
        GameRegistry.registerItem(this, getName());
	}
	
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister)
    {
        this.itemIcon = par1IconRegister.registerIcon(ModInfo.MODID + ":" + TextureLocation() + getName());
    }
}

