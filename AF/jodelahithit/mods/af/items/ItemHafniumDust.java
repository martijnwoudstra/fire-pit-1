package jodelahithit.mods.af.items;

import jodelahithit.mods.af.AF;
import jodelahithit.mods.af.lib.IAF;
import jodelahithit.mods.af.lib.Ref;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHafniumDust extends Item implements IAF {

	public ItemHafniumDust() {
		super();
	}
	
    @Override
    public String getName()
    {
        return Ref.NameItemHafniumDust;
    }

	@Override
	public String TextureLocation() {
		return "ores/";
	}

}
