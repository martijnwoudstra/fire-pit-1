package jodelahithit.mods.af.items;


import jodelahithit.mods.af.AF;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTantalumIngot extends Item {

	public ItemTantalumIngot() {
		super();

		this.setCreativeTab(AF.CT);

	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconregister) {
		this.itemIcon = iconregister.registerIcon(AF.modid + ":" + "ingots/" + this.getUnlocalizedName().substring(5));
	}

}
