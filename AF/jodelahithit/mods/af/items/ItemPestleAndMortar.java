package jodelahithit.mods.af.items;


import jodelahithit.mods.af.AF;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemPestleAndMortar extends Item{

	public ItemPestleAndMortar() {
		super();
		setMaxDamage(15);
		setMaxStackSize(1);

		this.setCreativeTab(AF.CT);

	}
	
	public boolean isDamageable()
	{
	    return true;
	}

	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconregister) {
		this.itemIcon = iconregister.registerIcon(AF.modid + ":" + "misc/" + this.getUnlocalizedName().substring(5));
	}
	
	public boolean hasContainerItem()
	{
	    return true;
	}
	

	public ItemStack getContainerItemStack(ItemStack is)
	  {
		
	    is = is.copy();
	    is.setItemDamage(is.getItemDamage() + 1);
	    if (is.getItemDamage() > getMaxDamage()) {
	      is.stackSize = 0;
	    }
	    return is;
	  }
	  
	 public boolean doesContainerItemLeaveCraftingGrid(ItemStack par1ItemStack)
	 {
	    return false;
	 }
}


