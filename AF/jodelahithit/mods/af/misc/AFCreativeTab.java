package jodelahithit.mods.af.misc;

import java.io.File;

import jodelahithit.mods.af.items.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AFCreativeTab extends CreativeTabs {

	public AFCreativeTab(int i, String string) {
		super(i, string);
	}

	public String getTranslatedTabLabel() {
		return "Additional Furnaces";
		
	}

	@Override
	public Item getTabIconItem() {
		return ModItems.HafniumDust;

	}
}