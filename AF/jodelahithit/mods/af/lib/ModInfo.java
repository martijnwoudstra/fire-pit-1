package jodelahithit.mods.af.lib;

public class ModInfo {

	public static final String MODID = "AF";
	public static final String NAME = "Additional Furnaces";
	public static final String VERSION = "0.1-1.6.4";
	public static final String CLIENTSIDE = "jodelahithit.mods.af.proxy.AFClientProxy";
	public static final String SERVERSIDE = "jodelahithit.mods.af.proxy.CommonProxy";
}
