package jodelahithit.mods.af.block;

import net.minecraft.block.Block;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ModBlocks {

    public static Block OreTantalum;
    public static Block OreHafnium;
    public static Block HTCFurnaceActive;
    public static Block HTCFurnaceIdle;

    public static void init()
    {
        OreTantalum = new OreTantalum();
        OreHafnium = new OreHafnium();
//      HTCFurnaceActive = new BlockHTCFurnace(true).setUnlocalizedName("AF_HTCFurnaceActive");
//      HTCFurnaceIdle = new BlockHTCFurnace(false).setUnlocalizedName("AF_HTCFurnaceIdle");
     
        RB(OreTantalum, "Tantalum Ore");
        RB(OreHafnium, "Hafnium Ore");
//        RB(HTCFurnaceActive, "Furnace Active");
//        RB(HTCFurnaceIdle, "Furnace Idle");

    }
    public static void RB(Block block, String name) {
        GameRegistry.registerBlock(block, name);
    }
}
